package ttLabs.foundations.Java;

import java.util.List;

public class ListSum {
    public static int listSum(List<Integer> list)
    {
        return list.stream().reduce(0, (subtotal, elem) -> subtotal+elem);
    }

    public static void main(String[] args) {
        List<Integer> list = List.of(new Integer[]{1,2,3});

        System.out.println(listSum(list));
    }
}
