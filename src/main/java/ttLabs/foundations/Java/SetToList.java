package ttLabs.foundations.Java;

import java.util.List;
import java.util.Set;

public class SetToList {
    public static List<Integer> setToList(Set<Integer> set)
    {
        return set.stream().toList();
    }
}
