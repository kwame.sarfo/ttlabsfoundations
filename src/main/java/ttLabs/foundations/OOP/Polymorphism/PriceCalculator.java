package ttLabs.foundations.OOP.Polymorphism;

interface PriceCalculator {

    double calculate();
}
