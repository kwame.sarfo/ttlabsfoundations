package ttLabs.foundations.OOP.Polymorphism;

class AgencyOne implements PriceCalculator{

    AgencyOne() {};

    @Override
    public double calculate() {
        return 5;
    }
}
