package ttLabs.foundations.OOP.Polymorphism;

public class CarLender {
    PriceCalculator priceCalculator;

    CarLender(PriceCalculator priceCalculator){
        this.priceCalculator = priceCalculator;
    };

    double getAmountToPay()
    {
        return this.priceCalculator.calculate();
    }

    public void setPriceCalculator(PriceCalculator priceCalculator) {
        this.priceCalculator = priceCalculator;
    }
}
