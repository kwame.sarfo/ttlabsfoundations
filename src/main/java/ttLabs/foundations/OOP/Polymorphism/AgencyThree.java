package ttLabs.foundations.OOP.Polymorphism;

public class AgencyThree implements PriceCalculator{

    AgencyThree(){};

    @Override
    public double calculate() {
        return 10;
    }
}
