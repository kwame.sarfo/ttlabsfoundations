package ttLabs.foundations.OOP.SOLID.LiskovSubstitutionPrinciple;

 class TransferableAccount extends Account{

   TransferableAccount() {}

   String transfer() {
    return "transfer";
  }
}
