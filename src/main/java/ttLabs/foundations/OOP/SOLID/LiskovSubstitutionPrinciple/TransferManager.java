package ttLabs.foundations.OOP.SOLID.LiskovSubstitutionPrinciple;


class TransferManager {

    //an example of Liskov Substitution principle violation since a subclass of account that does not
    //support transfers can not be substituted for account
//    private Account account;
//
//    TransferManager(Account account)
//    {
//        this.account = account;
//    }
//
//    String performTransfer()
//    {
//        return this.account.transfer();
//    }


    //an example of adherence to Liskov Substitution principle
    private TransferableAccount account;

    TransferManager(TransferableAccount account)
    {
        this.account = account;
    }

    String performTransfer()
    {
        return this.account.transfer();
    }
}
