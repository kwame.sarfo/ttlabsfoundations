package ttLabs.foundations.OOP.SOLID.OpenClosedPrinciple;

 interface Dateable {
    int getCreationDay();
}
