package ttLabs.foundations.OOP.SOLID.OpenClosedPrinciple;

import java.time.LocalDate;

 class Account implements Dateable {

     private LocalDate createdAt;

     Account()
    {
        this.createdAt = LocalDate.now();
    }

     @Override
     public int getCreationDay() {
         return createdAt.getDayOfYear();
     }
 }


