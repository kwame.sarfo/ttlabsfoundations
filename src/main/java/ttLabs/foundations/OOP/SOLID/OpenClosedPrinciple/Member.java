package ttLabs.foundations.OOP.SOLID.OpenClosedPrinciple;

import java.util.Calendar;

public class Member implements Dateable {
    private int createdAt;

     Member()
    {
        this.createdAt = Calendar.DAY_OF_YEAR;
    }

    @Override
    public int getCreationDay() {
        return this.createdAt;
    }
}
