package ttLabs.foundations.OOP.SOLID.OpenClosedPrinciple;


class CreationDayExtractor {

    //an example of Open/Closed principle violation since the extractor only works with
    //account and not any other class that has a creation date
//    private Account account;
//
//    public CreationDayExtractor(Account account)
//    {
//        this.account = account;
//    }
//
//    public int extractCreationDayOfYear()
//    {
//        return this.account.getCreatedAt().getDayOfYear();
//    }


    //an example of adherence to open/closed principle
    private Dateable dateable;

    CreationDayExtractor(Dateable dateable)
    {
        this.dateable = dateable;
    }

    int extractCreationDayOfYear()
    {
        return this.dateable.getCreationDay();
    }
}
