package ttLabs.foundations.OOP.SOLID.DependencyInversionPrinciple;


class TransferManager {

    //an example of Dependency Inversion Principle violation since we are using a concrete
    //implementation in our manager
    //TranseferProvider1 TranseferProvider1;


    //an example of adherence to Dependency Inversion Principle
    TransferService transferService;

    TransferManager(TransferService transferService)
    {
        this.transferService = transferService;
    }


    String performTransfer()
    {
        return this.transferService.transfer();
    }
}
