package ttLabs.foundations.OOP.SOLID.DependencyInversionPrinciple;

interface TransferService {
    String transfer();
}
