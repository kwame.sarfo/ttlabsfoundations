package ttLabs.foundations.OOP.SOLID.DependencyInversionPrinciple;

import java.time.LocalDate;

 class Account {

    private LocalDate createdAt;

    Account()
    {
        this.createdAt = LocalDate.now();
    }

    String deposit() {
         return "deposit";
     }
 }


