package ttLabs.foundations.OOP.SOLID.InterfaceSegregationPrinciple;

interface AccountFunctionalities {
    String withdraw();
    String transfer();
}
