package ttLabs.foundations.OOP.SOLID.InterfaceSegregationPrinciple;

interface Transferable {
    String transfer();
}
