package ttLabs.foundations.OOP.SOLID.InterfaceSegregationPrinciple;

class TransferableAccount extends Account implements Transferable {

   TransferableAccount()
   {

   }

   @Override
   public String transfer() {
    return "transfer";
  }

//    //example of interface segregation principle violation because this class should not
//    //be forced to implement withdraw since it doesn't need it.
//    @Override
//    public String withdraw() {
//        return null;
//    }
}
