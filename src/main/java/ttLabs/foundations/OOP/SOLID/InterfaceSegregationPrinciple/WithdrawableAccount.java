package ttLabs.foundations.OOP.SOLID.InterfaceSegregationPrinciple;

class WithdrawableAccount extends Account implements Withdrawable{

    WithdrawableAccount()
    {

    }


    @Override
    public String withdraw() {
        return "withdraw";
    }

      //example of interface segregation principle violation because this class should not
//    //be forced to implement transfer since it doesn't need it.
//    @Override
//    public String transfer() {
//        return null;
//    }
}
