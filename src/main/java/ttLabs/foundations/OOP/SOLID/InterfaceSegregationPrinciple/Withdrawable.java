package ttLabs.foundations.OOP.SOLID.InterfaceSegregationPrinciple;

interface Withdrawable {
    String withdraw();
}
