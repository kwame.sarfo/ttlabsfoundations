package ttLabs.foundations.OOP.SOLID.SingleResponsibilityPrinciple;

 class CreationDayExtractor {
    private Account account;

    public CreationDayExtractor(Account account)
    {
        this.account = account;
    }

    public int extractCreationDayOfYear()
    {
        return this.account.getCreatedAt().getDayOfYear();
    }


    //example of violation of SRP
    public void print()
    {
        System.out.println("Account created on day " + extractCreationDayOfYear());
    }
}
