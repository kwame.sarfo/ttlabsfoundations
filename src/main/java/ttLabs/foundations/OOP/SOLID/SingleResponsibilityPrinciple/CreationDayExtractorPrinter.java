package ttLabs.foundations.OOP.SOLID.SingleResponsibilityPrinciple;

 class CreationDayExtractorPrinter {
    private CreationDayExtractor extracter;

    public CreationDayExtractorPrinter(CreationDayExtractor extracter)
    {
        this.extracter = extracter;
    }

    // example of adherence to Single Responsibility Principle
    public void print()
    {
        System.out.println("Account created on day " + this.extracter.extractCreationDayOfYear());
    }
}
