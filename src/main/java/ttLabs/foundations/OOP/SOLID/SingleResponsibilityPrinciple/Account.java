package ttLabs.foundations.OOP.SOLID.SingleResponsibilityPrinciple;

import java.time.LocalDate;

 class Account {

    private LocalDate createdAt;

    public Account()
    {
        this.createdAt = LocalDate.now();
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }
}


