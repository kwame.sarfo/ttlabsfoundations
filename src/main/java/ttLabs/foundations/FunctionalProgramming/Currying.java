package ttLabs.foundations.FunctionalProgramming;

import java.util.function.Function;

public class Currying {
    public static Function<Integer, Function<Integer, Function<Integer, Integer>>> curry = a -> b -> c -> a+b+c;


    public static void main(String[] args) {
        Function<Integer, Function<Integer, Integer>> partOne = curry.apply(1);

        Function<Integer, Integer> partTwo = partOne.apply(2);

        Integer partThree = partTwo.apply(3);

        System.out.println(partThree);
    }
}
