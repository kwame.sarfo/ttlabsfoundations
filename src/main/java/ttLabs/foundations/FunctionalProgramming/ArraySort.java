package ttLabs.foundations.FunctionalProgramming;

import java.util.Arrays;

public class ArraySort {
    public static int[] sort(int[] nums)
    {
        return Arrays.stream(nums).sorted().toArray();
    }

    public static void main(String[] args) {
        int[] nums = new int[]{3, 2, 1, 5};
        System.out.println(Arrays.toString(nums));
        sort(nums);
        System.out.println(Arrays.toString(nums));
    }
}
