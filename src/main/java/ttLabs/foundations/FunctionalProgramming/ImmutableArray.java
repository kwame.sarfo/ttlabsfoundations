package ttLabs.foundations.FunctionalProgramming;

import java.util.Arrays;

public class ImmutableArray<T> {
    private T[] immutableArray;

    ImmutableArray(T[] array)
    {
        this.immutableArray = Arrays.copyOf(array, array.length);
    }

    public T get(int index)
    {
        return immutableArray[index];
    }

    @Override
    public String toString() {
        return Arrays.toString(immutableArray);
    }
}