package ttLabs.foundations.FunctionalProgramming;

import java.util.function.Function;

public class Composition {
    public Function<Integer, Integer> addTwo = (num) -> num + 2;
    public Function<Integer, Integer> multiplyByThree = (num) -> num * 3;

    public int multiplyByThreeThenAddTwo(int num)
    {
        return addTwo.compose(multiplyByThree).apply(num);
    }

}
