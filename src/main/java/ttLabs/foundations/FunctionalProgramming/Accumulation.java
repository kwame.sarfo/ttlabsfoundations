package ttLabs.foundations.FunctionalProgramming;

import java.util.function.BiFunction;

public class Accumulation {

    public static BiFunction<int[], Integer, Integer> accumulatorFunction = (arr, index) ->  arr[index];

    public static int accumulate(int acc, int[] arr , BiFunction<int[], Integer, Integer> accumulatorFunction, int index)
    {
        if(index >= arr.length) return acc;
        else{
            return accumulate(acc + accumulatorFunction.apply(arr, index), arr, accumulatorFunction, index+1);
        }
    }

    public static void main(String[] args) {
        int[] nums = new int[]{3, 2, 1, 5};
        System.out.println(accumulate(0, nums, accumulatorFunction, 0));
    }
}
