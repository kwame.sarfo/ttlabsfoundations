package ttLabs.foundations.FunctionalProgramming;

public class CountElementsRecursion<T> {

    public int count(T[] arr)
    {
        return count(arr, 0);
    }

    private int count(T[] arr, int count)
    {
        try{
            T t = arr[count];
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return count;
        }
        return count(arr, count+1);
    }
}
