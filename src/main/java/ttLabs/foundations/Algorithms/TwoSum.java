package ttLabs.foundations.Algorithms;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    //time complexity is 0(n) since I loop from 0 to n(length of int[]) over the int[]
    //space complexity is 0(n) since the size of the hashmap increases as the input size increases
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> lookup = new HashMap<>();

        int[] results = new int[2];

        for(int i = 0; i<nums.length; i++)
        {
            int complement = target - nums[i];

            if(!lookup.containsKey(complement))
            {
                lookup.put(nums[i], i);
            }
            else
            {
                results[0] = i;
                results[1] = lookup.get(complement);
                break;
            }
        }

        return results;

    }
}
